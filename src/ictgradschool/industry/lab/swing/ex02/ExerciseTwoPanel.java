package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JTextField firstText ;
    private JTextField secondText;
    private JTextField thirdText;
    private JButton add;
    private JButton subtract;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        firstText = new JTextField(10);
        secondText = new JTextField(10);
        thirdText = new JTextField(20);
        add = new JButton("Add");
        subtract = new JButton("Subtract");

        JLabel result;
        result = new JLabel("Result: ");

        add(firstText);
        add(secondText);
        add(add);
        this.add(add);
        add.addActionListener(this);
        add(subtract);
        this.add(subtract);
        subtract.addActionListener(this);
        add(result);
        add(thirdText);


    }

    public void actionPerformed(ActionEvent event){
        if(event.getSource() == add){
            double firstNum = Double.parseDouble(firstText.getText());
            double secondNum = Double.parseDouble(secondText.getText());
            double addition = firstNum + secondNum;
            double round = roundTo2DecimalPlaces(addition);
            thirdText.setText(round + "");
        }

        if(event.getSource() == subtract){
            double firstNum = Double.parseDouble(firstText.getText());
            double secondNum = Double.parseDouble(secondText.getText());
            double subtraction = firstNum - secondNum;
            double round = roundTo2DecimalPlaces(subtraction);
            thirdText.setText(round + "");
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}